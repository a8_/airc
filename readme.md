# Airc

Airc is a IRC client made to make IRC easy to use.
the app is text based, you must run it in terminal


# Technical details
When you input the server ip to connect to it will automatically try to connect to the ip with port 6697 and try to use TLS/SSL, you cannot change this as of now.

Airc is powered by the [Kitteh irc](https://github.com/KittehOrg/KittehIRCClientLib/tree/master) irc bot library 




# usage
When you launch the app in terminal it will ask you for the username you want to use. once you input the username you want to use it will ask you for the IP of the irc server you want to connect to, once thats done you will be asked for the channel you want to connect to. Then you will be asked if you want to enable logging.

To send message just type it in console and press enter.

*Currently you cannot DM, change channels, or download files. All you can do is join one channel and chat.*


# copyright

        Airc a simple to use irc client made in java
        Copyright (C) 2020  a8_

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
