//        Airc a simple to use irc client made in java
//        Copyright (C) 2020  a8_
//
//        This program is free software: you can redistribute it and/or modify
//        it under the terms of the GNU General Public License as published by
//        the Free Software Foundation, either version 3 of the License, or
//        (at your option) any later version.
//
//        This program is distributed in the hope that it will be useful,
//        but WITHOUT ANY WARRANTY; without even the implied warranty of
//        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//        GNU General Public License for more details.
//
//        You should have received a copy of the GNU General Public License
//        along with this program.  If not, see <https://www.gnu.org/licenses/>.


import net.engio.mbassy.listener.Handler;
import org.fusesource.jansi.AnsiConsole;
import org.kitteh.irc.client.library.Client;
import org.kitteh.irc.client.library.element.User;
import org.kitteh.irc.client.library.event.channel.ChannelJoinEvent;
import org.kitteh.irc.client.library.event.channel.ChannelMessageEvent;
import org.kitteh.irc.client.library.event.channel.ChannelPartEvent;
import org.kitteh.irc.client.library.event.client.ClientNegotiationCompleteEvent;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class main {
   static boolean textansbool;
   static String filePath;
static PrintWriter myWriter;
static  FileWriter fWriter;
static String userChoiceName = "";
    public static class Listener {

        @Handler
        public static void ServerMessageEvent(ChannelMessageEvent event)  {
            String msg = event.getMessage();
            User user = event.getActor();
            String nick = user.getNick();
            String forlog = "[" + nick + "]" + " - " + msg;

            if(msg.contains(userChoiceName)){
                System.out.println( ConsoleColors.BLUE + "[" + nick + "]" + " - "  + msg + ConsoleColors.RESET);
            }else {
                System.out.println("[" + nick + "]" + " - " + msg);
            }

            if (textansbool){
                DateFormat dateFormat = new SimpleDateFormat("HH-mm:ss");
                Date date = new Date();


                myWriter.append("[" + dateFormat.format(date) + "]" + forlog);
                myWriter.println();
                myWriter.flush();




            }

        }



        @Handler
        public void onUserJoinChannel(ChannelJoinEvent event) {

            if (event.getClient().isUser(event.getUser())) { // It's me!
                return;
            }
            // It's not me!
            System.out.println(ConsoleColors.GREEN + event.getUser().getNick() + " Joined the chat" + ConsoleColors.RESET );

            if (textansbool){
                DateFormat dateFormat = new SimpleDateFormat("HH-mm:ss");
                Date date = new Date();


                myWriter.append("[" + dateFormat.format(date) + "]" + " " + event.getUser().getNick() + " Joined the chat");
                myWriter.println();
                myWriter.flush();


            }

        }

        @Handler
        public void userLeave (ChannelPartEvent event) {
            //TODO: fix if user timed out

            System.out.println(ConsoleColors.RED + event.getUser().getNick() + " left the chat" + ConsoleColors.RESET);

            if (textansbool){
                DateFormat dateFormat = new SimpleDateFormat("HH-mm:ss");
                Date date = new Date();


                myWriter.append("[" + dateFormat.format(date) + "]" + " " +  event.getUser().getNick() + " left the chat");
                myWriter.println();
                myWriter.flush();


            }
        }


        @Handler
        public void meow(ClientNegotiationCompleteEvent event) {
            System.out.println(ConsoleColors.GREEN + "Connected" + ConsoleColors.RESET);
            System.out.println("Just type the message you want to send and hit enter");
        }


    }




    public static void main(String[] args) {

        AnsiConsole.systemInstall();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please input the username you want to use");
        userChoiceName = scanner.nextLine();
        System.out.println();


        System.out.println("Please input the IP adress you want to connect to");
        System.out.println("Freenode is irc.freenode.net");
        String userChoice = scanner.nextLine();

        System.out.println();

        System.out.println("Please input the channel you want to join");
        String userChoiceChan = scanner.nextLine();

        System.out.println();

        System.out.println("Do you want to log the chat into a text file?");
        System.out.println("The text file will be yyyy/MM/dd HH:mm.txt ");
        System.out.println("Type true or false NOT yes or no"); //todo: allow yes or no
        String textAnsw = scanner.nextLine();
        textansbool = Boolean.parseBoolean(textAnsw);



        if (textansbool) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm");
            Date date = new Date();

            filePath = System.getProperty("user.dir").toString() + "/" + dateFormat.format(date)+".txt";
            File log = new File(filePath);
            try {
                fWriter = new FileWriter(filePath, true);
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                myWriter = new PrintWriter (new FileWriter(filePath));
            } catch (IOException e) {
                e.printStackTrace();
            }


        }




        System.out.println();

        Client client = Client.builder().nick(userChoiceName).server().host(userChoice).then().buildAndConnect();
        client.getEventManager().registerEventListener(new Listener());
        client.addChannel(userChoiceChan);
        System.out.println("Connecting. Please wait");







        do{
            String userMsg = scanner.nextLine();


            //fixes random error that happens for unknown resion
            if (!(userMsg == null)){
                client.sendMessage(userChoiceChan, userMsg);
                System.out.println("[" + userChoiceName + "]" + " - "  + userMsg);

                if (textansbool){
                    DateFormat dateFormat = new SimpleDateFormat("HH-mm:ss");
                    Date date = new Date();





                    myWriter.append("[" + dateFormat.format(date) + "]" + "[" + userChoiceName + "]" + " - "  + userMsg);
//                    System.out.println("Successfully wrote to the file.");
                    myWriter.println();
                    myWriter.flush();




                }

            }










        }while (true);




    }



}